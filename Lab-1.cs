using System;

namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myArr = new int[50];
            Random rand = new Random();


            #region Bubblesort
            Console.WriteLine("Array before sort");

            for (int i = 0; i < 50; i++)
            {
                myArr[i] = rand.Next(100);
                Console.Write(myArr[i] + " ");
            }

            BubbleSort(myArr);
            Console.WriteLine("\n\nArray after bubblesort");
            for (int i = 0; i < 50; i++)
            {
            Console.Write(myArr[i] + " ");
            }
            #endregion
		    
	    //Сортировка пузирьком
            static void BubbleSort(int[] arr)
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    for (int j = 0; j < arr.Length - i - 1; j++)
                    {
                        if (arr[j] > arr[j + 1])
                        {
                            int temp = arr[j];
                            arr[j] = arr[j + 1];
                            arr[j + 1] = temp;
                        }
                    }
                }
            }	
		
		    
	}
    }
}
